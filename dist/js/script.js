"use strict";
jQuery(function($)
{
	var choices = [];
	var arr = [];
	var suburbFull;
	var suburValue;
	var postValue;
	// radial indicator
	var radialObj = $('#borrowResultContainer').radialIndicator({
		barColor: '#ffbe3e',
		barWidth: 15,
		initValue: 85,
		displayNumber: false,
		roundCorner: false,
		percentage: true,
		radius: 75
	});	
	var radialObj2 = $('#loanResultContainer').radialIndicator({
		barColor: '#a7c125',
		barWidth: 15,
		initValue: 40,
		displayNumber: false,
		roundCorner: false,
		percentage: true,
		radius: 75
	});
	// Get GeoData 
	$.getJSON("dist/js/geodata.json", function(d) {
		$.each(d, function(name, val) {
			val[2] = val[2].length < 4 ? '0' + val[2] : val[2];
			arr.push("" + val.join(', '));
		});
		$('input[data-validate="address"]').autoComplete({
			minChars: 1,
			source: function source(term, suggest) {
				term = term.toLowerCase();
				var choices = arr;
				var matches = [];
				for (var i = 0; i < choices.length; i++) {
					if (choices[i].toLowerCase().indexOf(term) > -1)
						matches.push(choices[i]);
				}
				suggest(matches);
			},
			onSelect: function onSelect(e, term, item) {
				$(this).focus();
				$('.autocomplete-suggestions').hide();
			},
			cache: false
		});
	});
	var errorMsg, dynamicErrorMsg = function dynamicErrorMsg() {
		return errorMsg;
	};
	// address validator
	$.validator.addMethod("address", function(value, element) {
		return this.optional(element) || /^(.+),[\s]*(.+),[\s]*(\d{4})$/.test(value);
	}, "Please select your suburb from the drop down list after typing in your postcode.");
	// usd validator
	$.validator.addMethod("usd", function(value, element) {
		return this.optional(element) || /^\$?((\d{1,3}(,\d{3})*)|\d+)(\.(\d{2})?)?$/.test(value);
	}, "This field must be formatted as $ dollar amount");
	// Phone Validator
	$.validator.addMethod("phone", function(value, element){
		var result = true,
		elemval = $.trim(value);
		if(elemval !== ''){
			result = true;
		}
		return result;
	}, "Please enter phonenumber");	
	// Verif Validator
	$.validator.addMethod("verif", function(value, element){
		var result = true,
		elemval = $.trim(value);
		if(elemval !== ''){
			result = true;
		}
		return result;
	}, "Please enter phonenumber");
	// fullname validator
	$.validator.addMethod("fullname", function(value, element) {
		errorMsg = 'Please enter full name';
		var result = true, 
		elemval = $.trim(value), 
		fullname_test = elemval.split(' '), 
		patt = new RegExp(/^[a-zA-Z-' ]+$/), 
		firstname = '', 
		lastname = '', 
		placeholder = $(element).attr('placeholder');
		if (fullname_test.length < 2) {
			errorMsg = 'Please enter your first name and last name';
			result = false;
		} else if (elemval == placeholder) {
			errorMsg = 'Please enter full name';
			result = false;
		} else {
			lastname = fullname_test[fullname_test.length - 1];
			for (var i = 0; i < fullname_test.length - 1; i++) {
				if (i > 0) {
					firstname += ' ';
				}
				firstname += fullname_test[i];
			}
			if (firstname.length < 2 || lastname.length < 2) {
				errorMsg = 'Name under 2 characters';
				result = false;
			} else if (firstname == lastname) {
				errorMsg = 'Firstname and lastname identical';
				result = false;
			} else if (!patt.test(elemval)) {
				errorMsg = 'Please enter only alphabets';
				result = false;
			}
		}
		return result;
	}, dynamicErrorMsg);
	// input number validator
	$('input[type="tel"], input[type="number"]').keydown(function(e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 224, 17, 91, 93]) !== -1 || e.keyCode == 65 && e.ctrlKey === true || e.keyCode == 65 && e.metaKey === true || e.keyCode >= 35 && e.keyCode <= 39) {
			return;
		}
		if ((e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
		}
	});
	// add commas
	var addCommas = function addCommas(nStr) {
		nStr += '';
		var x = nStr.split('.')
		, x1 = x[0]
		, x2 = x.length > 1 ? '.' + x[1] : ''
		, rgx = /(\d+)(\d{3})/;
		while (rgx.test(x1)) {
			x1 = x1.replace(rgx, '$1' + ',' + '$2');
		}
		return x1 + x2;
	};
	// input dollar validator
	$('.input-dollar').on('keyup', function(e) {
		var amount = $(this).val().indexOf('$') > -1 ? $(this).val().slice(1) : $(this).val();
		amount = amount.split(',').join('');
		var fieldMax = $(this).attr('data-max-amount') ? $(this).attr('data-max-amount') : '2000000';
		if (parseInt(amount) < 1) {
			amount = '';
		}
		if (amount.length > fieldMax.length) {
			amount = amount.slice(0, fieldMax.length);
		}
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 224, 17, 91, 93]) !== -1 || e.keyCode == 65 && e.ctrlKey === true || e.keyCode == 65 && e.metaKey === true || e.keyCode >= 35 && e.keyCode <= 39) {
			return;
		}
		$(this).val(addCommas(amount));
	});
	// input dollar blur
	$('.input-dollar').on('blur', function() {
		var elem = $(this);
		var moneyVal = $(this).val().indexOf('$') > -1 ? $(this).val().slice(1) : $(this).val();
		moneyVal = moneyVal !== '' ? parseInt(elem.val().replace(/[\D\s\._\-]+/g, "")) : 0;
		var fieldMax = $(this).attr('data-max-amount') ? parseInt($(this).attr('data-max-amount')) : 2000000;
		var fieldMin = $(this).attr('data-min-amount') ? parseInt($(this).attr('data-min-amount')) : 0;
		moneyVal = moneyVal > fieldMax ? fieldMax : moneyVal;
		if (moneyVal < fieldMin && moneyVal > 0) {
			elem.val(addCommas(fieldMin));
		} else if (moneyVal !== 0 && elem.val !== '') {
			elem.val(addCommas(moneyVal));
		}
	});
	var hideSteps = function hideSteps() {
		$('.tooltip').remove();
		$('.step').removeClass('form-active animated fadeIn');
	};
	$('.step').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
		$(this).removeClass('animated fadeIn');
	});

	var hashCheck = function hashCheck(cb) {
		var stepInt = parseInt(location.hash.replace('#step', ''));
		progessBar(location.hash);
		console.log("locationHash " + location.hash);
		console.log("StepInt " + stepInt);
		console.log("StepCount " + step_count);
		$('.step').removeClass('form-active');
		if (location.hash !== '#') {
			console.log("locationHashLength "+(location.hash).length);
			if ((location.hash).length > 0) {
				console.log(location.hash);
				$(location.hash).addClass('form-active animated fadeIn');
			} else {
				console.log("Ke Main");
				window.location.href="/";
			}
		} else {
			console.log("Ke Main");
			window.location.href="/";
		}
		cb;
		setTimeout(function() {
			$('html, body').animate({
				'scrollTop': 0
			}, 300, 'swing');
		}, 50);
	};

	var progessBar = function progessBar(progress) {
		var progressb = parseInt(progress.replace('#step', '')), 
		step_percent = 100 / step_count, 
		step_width = (progressb-1) * step_percent + '%';
		console.log("progressBarWidth " +step_width);
		$('.progress-bar').css({
			'width': step_width
		});
	};

	var conditional_next = function conditional_next(element) {
		console.log(element);
		if (element.parents('.form-radio-option').attr('data-condition') !== undefined || element.attr('data-condition') !== undefined) {
			var data_condition = element.parents('.form-radio-option').attr('data-condition') !== undefined ? element.parents('.form-radio-option').attr('data-condition') : element.attr('data-condition');

			console.log("data_condition "+data_condition);
			if (element.hasClass('submit-btn')) {
				data_condition = element.attr('data-condition');
			}
		}
	};

	var validateMyAjaxInputs = function validateMyAjaxInputs(step, stepid) {
		var rules = {}, 
		messages = {}, 
		tooltip_options = {}, 
		result = false;
		console.log("MASUK VALIDASI");

		var validator = $(stepid).parents('form').length > 0 ? $(stepid).parents('form') : $(stepid);

		$(stepid).find('input:not([type="hidden"]):not([type="radio"]):not([type="submit"]):not([type="checkbox"]), select').each(function(i, value) {
			var validationformat = $(this).attr('data-validate') !== undefined ? $(this).attr('data-validate') : 'require', 
			validationmessage = $(this).attr('data-msg') !== undefined ? $(this).attr('data-msg') : '', 
			field_name = $(this).attr('name');

			console.log("validationformat "+validationformat);
			console.log("validationmessage "+validationmessage);
			console.log("field_name " +field_name);

			rules[field_name] = {
				required: true
			};
			if ($(this).hasClass('ignore')) {
				rules[field_name] = {
					required: false
				};
			}
			if (validationformat !== undefined && validationformat !== 'require')
				rules[field_name][validationformat] = true;
			if (validationmessage !== '')
				messages[field_name] = validationmessage;
			tooltip_options[field_name] = {
				placement: 'bottom'
			};
		});
		validator.validate({
			rules: rules,
			messages: messages,
			tooltip_options: tooltip_options,
			invalidHandler: function invalidHandler(form, validator) {
				$('html, body').animate({
					scrollTop: $(validator.errorList[0].element).offset().top - 50
				}, 100);
			}
		});

		if ($(stepid).find('input[data-validate="usd"]').length > 0) {
			$(stepid).find('input[data-validate="usd"]').each(function(i, value) {
				var elem = $(this), 
				attr_placeholder = elem.attr('data-placeholder'), 
				field_val = elem.val(), 
				attr_maxlength = typeof elem.attr('maxlength') !== 'undefined' ? parseInt(elem.attr('maxlength')) : 10;

				if (field_val == attr_placeholder || field_val == '') {
					$(this).rules("add", {
						usd: true,
						maxlength: attr_maxlength,
						messages: {
							maxlength: function maxlength() {
								return field_val == attr_placeholder ? 'This field is required' : 'This field must be formatted as $ dollar amount';
							}
						}
					});
				}
			});
		};
		if ($(stepid).find('input[data-validate="fullname"]').length > 0) {
			$(stepid).find('input[data-validate="fullname"]').each(function(i, value) {
				console.log("MASUK FULLNAME");
				$(this).rules("add", {
					fullname: true,
					required: true,
					messages: {
						required: "Please enter fullname" 
					}
				});
			});
		};
		if ($(stepid).find('input[data-validate="phone"]').length > 0) {
			$(stepid).find('input[data-validate="phone"]').each(function(i, value) {
				$(this).rules("add", {
					required: true,
					messages: {
						required: "Please enter phonenumber" 
					}
				});
			});
		};
		if ($(stepid).find('input[data-validate="verif"]').length > 0) {
			$(stepid).find('input[data-validate="verif"]').each(function(i, value) {
				$(this).rules("add", {
					required: true,
					messages: {
						required: "Please enter verifnumber" 
					}
				});
			});
		};
		if ($(stepid).find('input[data-validate="email"]').length > 0) {
			$(stepid).find('input[data-validate="email"]').each(function(i, value) {
				$(this).rules("add", {
					required: true,
					messages: {
						required: "Please enter email" 
					}
				});
			});
		};
		if (validator.valid())
			result = true;
		else
			result = false;

		return result;
	};

	$('.submit-btn').click(function(e)
	{
		e.preventDefault();
		var stepopen='#step'+(parseInt($('.step.form-active').attr('id').split('step')[1])+1);
		if($(this).attr('data-next'))
			stepopen=$(this).attr('data-next');

		var steptovalidate='#'+$('.step.form-active').attr('id');
		console.log("stepopen submit " +stepopen);
		if(validateMyAjaxInputs(stepopen,steptovalidate)){
			console.log("stepopen after call " +stepopen);
			console.log("steptovalidate after call " +steptovalidate);
			window.location.hash=stepopen;
		}

	});

	$('.radio-option-btn').click(function(){
		var stepopen = $(this).parents('.form-radio-option').length>0?$(this).parents('.form-radio-option').attr('data-next'):'#step1';

		if($(this).siblings('input').is(':checked'))
		{
			console.log("input checked");
			if(conditional_next($(this))!==undefined)
			{
				if(conditional_next($(this))===1)
				{
					stepopen=stepopen.split(',')[0];
				}
				else{
					stepopen=stepopen.split(',')[1];
				}
			}

			var stepDelay=$(this).parents('.form-radio-option').hasClass('check-sign')?500:0;
			setTimeout(function(){window.location.hash=stepopen;},stepDelay);
		}
	});

	var step_count = $('.step').length;
	$(window).on('hashchange', function (e) {
		if (!$('body').hasClass('body-form')) {
			hashCheck();
		}
		if(location.hash==='#step2')
		{
			window.setTimeout(function(){
				window.location.hash = "#step3";
				$('#step2').addClass('nonactive').removeClass('form-active');
				$('.hla-header').removeClass('nonactive');
				$('.hla-navbar').removeClass('nonactive');
				$('.hla-footer').removeClass('nonactive');
				$('.first-step').removeClass('nonactive');
				$('.hla-header--progress').removeClass('nonactive');
			}, 5000);
		}
		else if(location.hash==='#step4'||location.hash==='#step5'||location.hash==='#step6'||location.hash==='#step7'||location.hash==='#step8'||location.hash==='#step11'||location.hash==='#step12'||location.hash==='#step13'||location.hash==='#step14'||location.hash==='#step15'){
			$('#header-title').text('Let’s start by calculating how much you need to borrow');
		}
		else if(location.hash==='#step3'||location.hash==='#step9'||location.hash==='#step10')
		{
			$('#header-title').html('Ok, you want to buy a <strong>home or move</strong> – that’s exciting.');
		}
		else if(location.hash==='#step16')
		{
			$('#header-title').html('Now check your <strong>phone.</strong>');
			$('#verificationModal').modal('show')
		}
		else if(location.hash==='#step17')
		{
			$('.hla-header-white').addClass('nonactive').removeClass('form-active');
			$('.hla-header-main.header-big').addClass('form-active').removeClass('nonactive');
		}
		else if(location.hash==='#step18')
		{	
			$('.hla-form').addClass('form-grey');
			$('.header-title').html('Thank you...');
			$('.header-subtitle').html('Thanks for leaving those details - we may give you a quick ring to ask a few extra questions to provide you with other home loan options that may be relevant to you.');
			$('.hla-header-white').addClass('nonactive').removeClass('form-active');
			$('.hla-header-main.header-big').addClass('nonactive').removeClass('form-active');
			$('.hla-header-main.last-step').removeClass('nonactive').addClass('form-active');
		}
		else{
			$('.hla-form').removeClass('form-grey');
			$('#step2').addClass('nonactive');
			$('.hla-header').removeClass('nonactive');
			$('.hla-footer').removeClass('nonactive');
			$('.hla-navbar').removeClass('nonactive');
			$('.first-step').removeClass('nonactive');
			$('.hla-header--progress').removeClass('nonactive');
			$('.header-title').text('Now we need to know a little bit about you');
		}
	});

	if (window.location.hash) {
		if(location.hash==='#step2'){
			window.setTimeout(function(){
				window.location.hash = "#step3";
				$('#step2').addClass('nonactive').removeClass('form-active');
				$('.hla-header').removeClass('nonactive');
				$('.hla-navbar').removeClass('nonactive');
				$('.hla-footer').removeClass('nonactive');
				$('.first-step').removeClass('nonactive');
				$('.hla-header--progress').removeClass('nonactive');
			}, 5000);
		}
		else{
			$('#step2').addClass('nonactive');
			$('.hla-header').removeClass('nonactive');
			$('.hla-footer').removeClass('nonactive');
			$('.hla-navbar').removeClass('nonactive');
			$('.first-step').removeClass('nonactive');
			$('.hla-header--progress').removeClass('nonactive');
			$('.header-title').text('Now we need to know a little bit about you');
		}
		$(window).trigger('hashchange')
	}
});