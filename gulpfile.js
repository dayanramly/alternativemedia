var gulp            = require('gulp'),
    browserSync     = require('browser-sync').create(),
    sass            = require('gulp-sass'),
    autoprefixer    = require('gulp-autoprefixer'),
    uglify          = require('gulp-uglify'),
    rename          = require('gulp-rename'),
    clean           = require('gulp-clean'),
    cleanCss        = require('gulp-clean-css'),
    csso            = require('gulp-csso'),
    sourcemaps      = require('gulp-sourcemaps'),
    runSequence     = require('run-sequence'),
    pipeline        = require('readable-stream').pipeline;


//ionicons
gulp.task('ioniconsFont', function(){
    return gulp.src('node_modules/ionicons/dist/fonts/*')
        .pipe(gulp.dest("dist/fonts"));
})

gulp.task('ioniconsCss', function(){
    return gulp.src('node_modules/ionicons/dist/scss/ionicons.scss')
        .pipe(sass())
        .pipe(csso())
        .pipe(rename("ionicons.min.css"))
        .pipe(gulp.dest("dist/css/"));
})

// Fonts
gulp.task('mainFonts', function(){
    return gulp.src('src/fonts/**/*')
        .pipe(gulp.dest("dist/fonts"));
})

//image
gulp.task('image', function(){
    return gulp.src(['src/img/*', 'src/img/*.png', 'src/scss/vendor/*.jpg'])
        .pipe(gulp.dest("dist/img/"));
})


// Compile sass into CSS & auto-inject into browsers
gulp.task('bootstrapSass', function() {
    return gulp.src(['node_modules/bootstrap/scss/bootstrap.scss'])
        .pipe(sass())
        .pipe(csso())
        .pipe(rename("bootstrap.min.css"))
        .pipe(gulp.dest("dist/css"))
        .pipe(browserSync.stream());
});

//compile style task
gulp.task('mainSass', function() {
	return gulp.src(['src/scss/style.scss', 'src/scss/*.scss'])
		.pipe(sass())
		.pipe(gulp.dest("dist/css"))
        .pipe(browserSync.stream());
})

// Compile sass into CSS & Minify
gulp.task('miniSass', function() {
    return gulp.src(['src/scss/style.scss', 'src/scss/*.scss'])
        .pipe(sass())
        .pipe(csso())
        .pipe(rename("style.min.css"))
        .pipe(gulp.dest("dist/css"))
        .pipe(browserSync.stream());
});

// Move the javascript files into our /src/js folder
gulp.task('js', function() {
    return gulp.src('src/js/script.js')
        .pipe(gulp.dest("dist/js"))
        .pipe(browserSync.stream());
});

gulp.task('minJs', function() {
    return pipeline(
        gulp.src(['src/js/*.js', 'src/js/*.min.js', 'src/js/**/*.min.js', 'src/js/*.json']),
        gulp.dest('dist/js')
    );
});

// Static Server + watching scss/html files
gulp.task('serve', ['mainSass'], function() {

    browserSync.init({
        server: "./"  
    });

    gulp.watch(['src/scss/style.scss', 'src/scss/pages/*.scss', 'src/scss/partials/*.scss', 'src/scss/*.scss', 'src/scss/**/*.scss'], ['mainSass']);
    gulp.watch("src/js/*.js", ['js'])
    gulp.watch("*.html").on('change', browserSync.reload);
});

gulp.task('default', ['js','serve']);

//clean dist folder
gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

gulp.task('build', ['clean'], function () {
  runSequence(
    'ioniconsFont',
    'ioniconsCss',
    'mainFonts',
    'image',
    'bootstrapSass',
    'mainSass',
    'miniSass',
    'js',
    'minJs'
  );
});